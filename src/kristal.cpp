/*
 * This sketch sends a UDP message over ETH from a ESP32 device to a IP adress.
 * Destination IP adress, UDP port and messages are defined in a file
 *
 * Based on the arduino-esp32
 * examples ETH_LAN8720 and WiFiUDPClient from espressif.
 *
 * https://github.com/espressif/arduino-esp32/tree/master/libraries/WiFi examples
 *
 * And Arduino's State Change Detection (Edge Detection) for pushbuttons
 * example code: https://www.arduino.cc/en/Tutorial/StateChangeDetection
 *
 */
#include <ETH.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

// IP address to send UDP data to:
// either use the ip address of the server or
// a network broadcast address

#define IPSHOWCTL "10.143.1.1"
#define COMPONENT "kristal_cpt_1"
#define UDPPORT 7000
#define startkristalscene "startkristalscene"

const char * udpAddress = IPSHOWCTL;
const int udpPort = UDPPORT;
const char * udpMessage1 = startkristalscene;

// this constant won't change:
const int sensor = 2;  // the pin that the sensor is attached to

// Variables will change:
int state = 1;      // current state of the sensor
int laststate = 1;    // previous state of the sensor

//The udp library class
WiFiUDP udp;

static bool eth_connected = false;

void WiFiEvent(WiFiEvent_t event)
{
  switch (event) {
    case SYSTEM_EVENT_ETH_START:
      Serial.println("ETH Started");
      //set eth hostname here
      ETH.setHostname("esp32-ethernet");
      break;
    case SYSTEM_EVENT_ETH_CONNECTED:
      Serial.println("ETH Connected");
      break;
    case SYSTEM_EVENT_ETH_GOT_IP:
      Serial.print("ETH MAC: ");
      Serial.print(ETH.macAddress());
      Serial.print(", IPv4: ");
      Serial.print(ETH.localIP());
      if (ETH.fullDuplex()) {
        Serial.print(", FULL_DUPLEX");
      }
      Serial.print(", ");
      Serial.print(ETH.linkSpeed());
      Serial.println("Mbps");
      eth_connected = true;
      break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:
      Serial.println("ETH Disconnected");
      eth_connected = false;
      break;
    case SYSTEM_EVENT_ETH_STOP:
      Serial.println("ETH Stopped");
      eth_connected = false;
      break;
    default:
      break;
  }
}

void setup()
{
  // initialize the sensor as a input:
  pinMode(sensor, INPUT_PULLUP);

  // initialize serial communication:
  Serial.begin(9600);

  delay(1000);

  WiFi.onEvent(WiFiEvent);
  ETH.begin();

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("OTA Ready");

}

void loop() 
{
  //only send data when connected
  if (eth_connected) {
    
    ArduinoOTA.handle();

    // read the pushbutton input pin:
    state = digitalRead(sensor); 

    // compare the State to its previous state, if the current state is LOW:
    if (state != laststate && state == LOW) {
      Serial.println(udpMessage1);
      udp.beginPacket(udpAddress,udpPort);
      Serial.println(udpMessage1);
      udp.printf(udpMessage1);
      udp.endPacket();
      }
      
    // Store current state in last state:
    laststate = state;

    } 
  
    // Delay a little bit to avoid bouncing:
    delay(10);
}
